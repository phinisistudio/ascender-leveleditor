/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leveleditor;

import java.util.*;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.tree.DefaultElement;

/**
 *
 * @author phinisicrewa
 */
public class Monster {
    
    public Monster(String name){
        this.name=name;
        this.data=new HashMap<String, String>();
    }
    
    public String getData(String key){
        return data.get(key);
    }
    
    public void addData(String key, String value){
        data.put(key, value);
    }
    
    public void deleteData(String key){
        data.remove(key);
    }
    
    public void addToDocument(Document doc){
        List<Node> monsters=doc.selectNodes("//monsters");
        if(monsters.isEmpty()){
            Element elmt=new DefaultElement("monsters");
            doc.getRootElement().add(elmt);
            monsters=doc.selectNodes("//monsters");
        }
        Element monster=new DefaultElement("monster")
        .addAttribute("name", name)
        .addAttribute("className", className)
        .addAttribute("preview", preview);
        for(String key:data.keySet()){
            monster.addAttribute(key, data.get(key));
        }
        Element element=(Element)monsters.get(0);
        element.add(monster);
    }
    
    public void removeFromDocument(Document doc){
        Node monster=doc.selectSingleNode("//monster[@name='"+name+"']");
        monster.detach();;
    }
    
    public static List<Monster> getFromDocument(Document doc){
        List<Monster> monsters=new ArrayList<Monster>();
        List<Node> nodes=doc.selectNodes("//monster");
        for(Node n:nodes){
            Element e=(Element)n;
            Monster m=new Monster(e.attributeValue("name"));
            m.className=e.attributeValue("className");
            m.preview=e.attributeValue("preview");
            for(Iterator i=e.attributeIterator();i.hasNext();){
                Attribute attr=(Attribute) i.next();
                if(!(attr.getName().equalsIgnoreCase("name")||attr.getName().equalsIgnoreCase("className")||attr.getName().equalsIgnoreCase("preview"))){
                    m.data.put(attr.getName(), attr.getValue());
                }
            }
            monsters.add(m);
        }
        return monsters;
    }
    
    public String name;
    public String className;
    public String preview;
    public Map<String,String> data;
}
