/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leveleditor;

import java.util.*;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.tree.DefaultElement;

/**
 *
 * @author phinisicrewa
 */
public class TileObject {
    public TileObject(String name){
        this.name=name;
        this.data=new HashMap<String, String>();
    }
    
    public String getData(String key){
        return data.get(key);
    }
    
    public void addData(String key, String value){
        data.put(key, value);
    }
    
    public void deleteData(String key){
        data.remove(key);
    }
    
    public void addToDocument(Document doc){
        List<Node> group=doc.selectNodes("//"+groupName+"s");
        if(group.isEmpty()){
            Element elmt=new DefaultElement(groupName+"s");
            doc.getRootElement().add(elmt);
            group=doc.selectNodes("//"+groupName+"s");
        }
        Element object=new DefaultElement(groupName)
        .addAttribute("name", name)
        .addAttribute("className", className)
        .addAttribute("preview", preview);
        for(String key:data.keySet()){
            object.addAttribute(key, data.get(key));
        }
        Element element=(Element)group.get(0);
        element.add(object);
    }
    
    public void removeFromDocument(Document doc){
        Node object=doc.selectSingleNode("//"+groupName+"[@name='"+name+"']");
        object.detach();
    }
    
    public static List<TileObject> getFromDocument(Document doc,String groupName){
        List<TileObject> group=new ArrayList<TileObject>();
        if(groupName==null || doc==null){
            return group;
        }
        List<Node> nodes=doc.selectNodes("//"+groupName);
        for(Node n:nodes){
            Element e=(Element)n;
            TileObject m=new TileObject(e.attributeValue("name"));
            m.groupName=groupName;
            m.className=e.attributeValue("className");
            m.preview=e.attributeValue("preview");
            for(Iterator i=e.attributeIterator();i.hasNext();){
                Attribute attr=(Attribute) i.next();
                if(!(attr.getName().equalsIgnoreCase("name")||attr.getName().equalsIgnoreCase("className")||attr.getName().equalsIgnoreCase("preview"))){
                    m.data.put(attr.getName(), attr.getValue());
                }
            }
            group.add(m);
        }
        return group;
    }
    
    public static TileObject getFromDocument(Document doc, String groupName, String className){
        groupName=groupName==null?"":groupName;
        className=className==null?"":className;
        Node node=doc.selectSingleNode("//*[@layerName='"+groupName+"']/*[@className='"+className+"']");
        if(node==null){
            return null;
        }
        Element e=(Element)node;
        TileObject m=new TileObject(e.attributeValue("name"));
        m.className=e.attributeValue("className");
        m.preview=e.attributeValue("preview");
        for(Iterator i=e.attributeIterator();i.hasNext();){
            Attribute attr=(Attribute) i.next();
            if(!(attr.getName().equalsIgnoreCase("name")||attr.getName().equalsIgnoreCase("className")||attr.getName().equalsIgnoreCase("preview"))){
                m.data.put(attr.getName(), attr.getValue());
            }
        }
        return m;
    }

    public Properties getProperties() {
        Properties pr=new Properties();
        pr.setProperty("name", name);
        pr.setProperty("className",className);
        for(String key: data.keySet()){
            pr.setProperty(key, data.get(key));
        }
        return pr;
    }
    
    public String name;
    public String className;
    public String preview;
    public String groupName;
    public Map<String,String> data;
}
