/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leveleditor;

import java.awt.Point;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import leveleditor.panel.LayerFrame;
import leveleditor.panel.MainFrame;
import leveleditor.panel.ObjectPickerFrame;
import leveleditor.panel.PalleteFrame;
import org.dom4j.DocumentException;

/**
 *
 * @author phinisicrewa
 */
public class LevelEditor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
         * Set the Nimbus look and feel
         */
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    option=new EditorOption("./leveleditor.xml");
                    
                    mainFrame=new MainFrame();
                    layerFrame=new LayerFrame();
                    objectPickerFrame=new ObjectPickerFrame();
                    palleteFrame=new PalleteFrame();
                    
                    mainFrame.setVisible(true);
                    layerFrame.setVisible(true);
                    objectPickerFrame.setVisible(true);
                    palleteFrame.setVisible(true);
                    
                    layerFrame.setLocation(new Point(mainFrame.getLocation().x+mainFrame.getSize().width,mainFrame.getLocation().y));
                    objectPickerFrame.setLocation(new Point(layerFrame.getLocation().x+layerFrame.getSize().width,layerFrame.getLocation().y));
                    palleteFrame.setLocation(new Point(mainFrame.getLocation().x,mainFrame.getLocation().y+mainFrame.getSize().height));
                    
                    objectPickerFrame.onOptionLoaded();
                } catch (IOException ex) {
                    Logger.getLogger(LevelEditor.class.getName()).log(Level.SEVERE, null, ex);
                } catch (DocumentException ex) {
                    Logger.getLogger(LevelEditor.class.getName()).log(Level.SEVERE, null, ex);
                }

                Runtime.getRuntime().addShutdownHook(new Thread(){
                    @Override
                    public void run(){
                        if(option!=null){
                            try {
                                option.saveDocument();
                            } catch (IOException ex) {
                                Logger.getLogger(LevelEditor.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                });
            }
        });
    }
    
    public static MainFrame getMainFrame(){
        return mainFrame;
    }
    
    public static LayerFrame getLayerFrame(){
        return layerFrame;
    }
    
    public static EditorOption getEditorOption(){
        return option;
    }
    
    public static ObjectPickerFrame getObjectPickerFrame(){
        return objectPickerFrame;
    }
    
    public static PalleteFrame getPalleteFrame(){
        return palleteFrame;
    }
    
    public static MainFrame mainFrame;
    public static LayerFrame layerFrame;
    public static EditorOption option;
    public static ObjectPickerFrame objectPickerFrame;
    public static PalleteFrame palleteFrame;
}
