/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leveleditor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.dom4j.io.SAXWriter;
import org.dom4j.tree.DefaultDocument;
import org.xml.sax.SAXException;

/**
 *
 * @author phinisicrewa
 */
public class EditorOption {
    
    public EditorOption(String filename) throws DocumentException, IOException{
        // create new if not exist
        this.filename=filename;
        File file=new File(filename);
        if(file.exists()){
            SAXReader reader=new SAXReader();
            doc=reader.read(filename);
        }
        else{
            doc=DocumentHelper.createDocument();
            doc.addElement("root");
            FileWriter writer=new FileWriter(filename);
            doc.write(writer);
            writer.close();
        }
    }
    
    public Document getDocument(){
        return doc;
    }
    
    public void saveDocument() throws IOException{
        FileWriter writer=new FileWriter(filename);
        doc.write(writer);
        writer.close();
    }
    
    public void loadDocument(String filename) throws DocumentException {
        SAXReader reader=new SAXReader();
        doc=reader.read(filename);
    }
    
    public List<Element> getObjectCollectionArray(){
        return doc.getRootElement().elements();
    }
    
    private String filename;
    private Document doc;
    
}
